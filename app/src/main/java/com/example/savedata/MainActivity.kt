package com.example.savedata

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Message
import android.preference.PreferenceManager
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val key = "MY_KEY"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        textViewWelcome.text = getString(R.string.welcome)

        //Obtenemos el preference manager
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)

        //Recuperamos las preferencias
        buttonGet.setOnClickListener {
            val myPref = prefs.getString(key, "No hay un valor para esta clave")
            if (myPref != null) {
                showAlert(myPref)
            }
        }
        //Guarda las preferencia
        buttonPut.setOnClickListener {
            //Modo edicion
            val editor = prefs.edit()
            editor.putString(key, "JEFRI")
            editor.apply()
            showAlert("Hemos guardado un valor")
        }
        //Borra las preferencias
        buttonDelete.setOnClickListener {
            //Modo edicion
            val editor = prefs.edit()
            editor.remove(key)
            editor.apply()
            showAlert("Hemos borrado un valor")

        }
    }

    private fun showAlert(message:String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("My preferences")
        builder.setMessage(message)
        val dialog = builder.create()
        dialog.show()
    }
}